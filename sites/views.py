from django.shortcuts import render
from sites.models import Site
from sites.models import Detail

# Create your views here.

def index(request):
    """サイト一覧画面"""

    if request.method == 'POST':
        site = Site.objects.create(
            title = request.POST['title'],
            url = request.POST['url'],
        )
    
    sites = Site.objects.all()
    date_dictionary = {'sites': sites}
    return render(request, 'sites/index.html', date_dictionary)

def detail(request):
    """"詳細画面"""

    if request.method == 'POST':
        detail = Detail.objects.create(
            username = request.POST['username'],
            password = request.POST['password'],
        )
    
    details = Detail.objects.all()
    date_dictionary = {'details': details}
    return render(request, 'sites/detail.html', date_dictionary)
    
def create(request):
    """登録画面"""
    return render(request, 'sites/create.html')

def login(request):
    """ログイン画面"""
    return render(request, 'sites/login.html')
