from django.urls import path
from . import views

urlpatterns = [
    #一覧画面
    path(r'', views.index, name="index"),
    #登録画面
    path(r'create', views.create, name="create"),
    #詳細画面
    path(r'detail', views.detail, name="detail"),
    #ログイン画面
    path(r'login', views.login, name="login"),
    #新規登録画面
]
