from django.db import models

# Create your models here
class Site(models.Model):
    """サイトのモデル"""
    title = models.CharField(max_length=256)
    url = models.CharField(max_length=256)

class Detail(models.Model):
    """詳細のモデル"""
    username = models.CharField(max_length=256)
    password = models.CharField(max_length=256)


    